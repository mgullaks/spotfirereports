from Spotfire.Dxp.Application.Filters import ListBoxFilter
from System.Collections.Generic import List
from Spotfire.Dxp.Data import *

# Make a list of all input parameters

list_dataControlTask = List[str]();
list_dataMachineType = List[str]();
list_dataProductSubType = List[str]();
list_dataProductType = List[str]();

# get a reference to a listbox

filtControlTask = Document.FilteringSchemes[Document.Data.Filterings["Filtering scheme"]][myDataTable][
    myDataTable.Columns["ControlTask"]].As[ListBoxFilter]()
filtMachineType = Document.FilteringSchemes[Document.Data.Filterings["Filtering scheme"]][myDataTable][
    myDataTable.Columns["MachineType"]].As[ListBoxFilter]()
filtProductSubType = Document.FilteringSchemes[Document.Data.Filterings["Filtering scheme"]][myDataTable][
    myDataTable.Columns["ProductSubType"]].As[ListBoxFilter]()
filtProductType = Document.FilteringSchemes[Document.Data.Filterings["Filtering scheme"]][myDataTable][
    myDataTable.Columns["Product_type"]].As[ListBoxFilter]()

# loop selected values
##get all possible values for ControlTask

if not filtControlTask.IncludeAllValues:
    for value in filtControlTask.SelectedValues:
        list_dataControlTask.Add(value)
else:
    cursor = DataValueCursor.CreateFormatted(myDataTable.Columns["ControlTask"])
    rows = filtMachineType.FilteredRows
    for row in myDataTable.GetRows(rows, cursor):
        value = cursor.CurrentValue
        if value <> str.Empty:
            list_dataControlTask.Add(value)

# get all possible values for MachineType
allMachineType = ''
if not filtMachineType.IncludeAllValues:
    for value in filtMachineType.SelectedValues:
        list_dataMachineType.Add(value)

else:
    allMachineType = ''
    cursor1 = DataValueCursor.CreateFormatted(myDataTable.Columns["MachineType"])
    for row in myDataTable.GetRows(cursor1):
        if allMachineType == '':
            allMachineType = "'" + str(cursor1.CurrentValue) + "'"
        elif allMachineType.find(
                cursor1.CurrentValue) == -1:  # if it is not found in the list already, so only unique values are recorded
            allMachineType += ', ' + "'" + str(cursor1.CurrentValue) + "'"

# get all possible values for ProductSubType
allProductSubType = ''
if not filtProductSubType.IncludeAllValues:
    for value in filtProductSubType.SelectedValues:
        list_dataProductSubType.Add(value)

else:
    allProductSubType = ''
    cursor1 = DataValueCursor.CreateFormatted(myDataTable.Columns["ProductSubType"])
    for row in myDataTable.GetRows(cursor1):
        if allProductSubType == '':
            allProductSubType = "'" + str(cursor1.CurrentValue) + "'"
        elif allProductSubType.find(
                cursor1.CurrentValue) == -1:  # if it is not found in the list already, so only unique values are recorded
            allProductSubType += ', ' + "'" + str(cursor1.CurrentValue) + "'"

# get all possible values for ProductType
allProductType = ''
if not filtProductType.IncludeAllValues:
    for value in filtProductType.SelectedValues:
        list_dataProductType.Add(value)
else:
    allProductType = ''
    cursor1 = DataValueCursor.CreateFormatted(myDataTable.Columns["Product_type"])
    for row in myDataTable.GetRows(cursor1):
        if allProductType == '':
            allProductType = "'" + str(cursor1.CurrentValue) + "'"
        elif allProductType.find(
                cursor1.CurrentValue) == -1:  # if it is not found in the list already, so only unique values are recorded
            allProductType += ', ' + "'" + str(cursor1.CurrentValue) + "'"

# print in away input infolink can read it  str(list_data).replace("List[str]","").replace("[","").replace("]","")
# Select which will be the assigned value to the document property CoontrolTask

Document.Properties["outputControlTask"] = "'{}'".format("', '".join(map(str, list_dataControlTask)))

# Select which will be the assigned value to the document property MachineType
if allMachineType == '':
    Document.Properties["outputMachineType"] = "'{}'".format("', '".join(map(str, list_dataMachineType)))
else:
    Document.Properties["outputMachineType"] = allMachineType

# Select which will be the assigned value to the document property Product SubType
if allProductSubType == '':
    Document.Properties["outputProductSubType"] = "'{}'".format("', '".join(map(str, list_dataProductSubType)))
else:
    Document.Properties["outputProductSubType"] = allProductSubType

# Select which will be the assigned value to the document property ProductType
if allProductType == '':
    Document.Properties["outputProductType"] = "'{}'".format("', '".join(map(str, list_dataProductType)))
else:
    Document.Properties["outputProductType"] = allProductType

# Refresh Data lake input table
infolink.Refresh()

# Show data
# Document.Properties["ClearSelection"] = 0

# Iterate over the pages and navigate to the page if it is named "Page Name"
for MyTab in Document.Pages:
    if (MyTab.Title == "Operations Management KPI's"):
        Document.ActivePageReference = MyTab



